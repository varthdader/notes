Space issue when we are trying to create docker images via build cmd

e.g.: Thin Pool has 4590 free data blocks which is less than minimum required 4863 free data blocks.

### Cleanup exited processes:
```script
docker rm $(docker ps -q -f status=exited)

```
### Cleanup dangling volumes:
```script
docker volume rm $(docker volume ls -qf dangling=true)
```
### Cleanup dangling images:
```script
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
```
### Remove all running containers
```script
docker rm --force $(docker ps -aq)
```
### Clear out all of the cached containers
```script 
docker rmi $(docker images -q)
```

### Copy files from <source> to <Destination> 
If the files are to be copied to/from the pods, you may use the oc tool to copy the file from source -> destination using ```rscync``` utility
```
oc rsync stecmanager-1-x5r61:/opt/Axway/SecureTransport/lib/certs/private/secret .
```

### Openshift's Haproxy Annotations for ST Web client loadbalancing
```
haproxy.router.openshift.io/balance: source
haproxy.router.openshift.io/disable_cookies: 'true'
haproxy.router.openshift.io/timeout: '10000'

```
